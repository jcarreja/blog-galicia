---
# Display name
title: Juan L Carreja

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Soy un hombre libre

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

#interests:
#- Historia
#- Filosofía
#- Viajes
#- Linux

#education:
#  courses:
#  - course: PhD in Artificial Intelligence
#    institution:
#    year: 2012
#  - course: MEng in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2009
#  - course: BSc in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
- icon: telegram
  icon_pack: fab
  link: https://t.me/juan_l_carreja
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "jcarreja@yahoo.es"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---
Vengo del país de la lluvia eterna.
Vengo del fin del mundo.
Soy de donde el agua muda la eternidad en melancolía y la melancolía se torna en nostalgia perenne.
Vengo de la más hermosa tierra que la naturaleza, en su eternidad, pudo parir.
Vengo de la piedra y el viento del norte, gélido y eterno.
Soy del país que los hombres llamaron Galicia.
Soy de la tierra que los dioses quisieron llamar ETERNIA.

